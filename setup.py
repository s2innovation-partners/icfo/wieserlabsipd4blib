import setuptools

setuptools.setup(
    name="WieserLabsIPD4Blib",
    version="1.0.0",
    description="Library for Wieserlabs WL-IPD4B photo diode",
    author="S2innovation",
    author_email="contact@s2innovation.com",
    license="Proprietary",
    classifiers=[
        'License :: Other/Proprietary License'],
    packages=setuptools.find_packages(),
    install_requires=['pyserial'],
    entry_points={
        'console_scripts': [
            "WieserLabsIPD4Blib=WieserLabsIPD4Blib:WieserLabsIPD4BController",
        ]
    }
)

