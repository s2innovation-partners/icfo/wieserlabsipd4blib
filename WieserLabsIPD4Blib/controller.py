import logging
from threading import Thread
from queue import LifoQueue
from time import sleep
import numpy as np

import serial

from WieserLabsIPD4Blib.resource import (
    WieserLabsIPD4BException, MSG_DICT
)


class WieserLabsIPD4BController:
    def __init__(self, symlink, result_number, commands):
        try:
            self.dev = serial.Serial(symlink, timeout=None, baudrate=1000000, rtscts=False)
        except:
            logging.error(f"Device {symlink} not found")
            raise WieserLabsIPD4BException(111)
        self.result_number = result_number

        for command in commands:
            self.send_command(command)
            sleep(1)

        self.result = {}

        for idx in range(self.result_number):
            self.result[idx] = []

        self.read_queue = LifoQueue()
        reading_queue = Thread(target=self.read_command)
        reading_queue.start()

    def decoding_result_queue(self, message):
        if b'D:' in message or b'D\x00:' in message:
            if b'L' in message:
                logging.warning("Results were dropped due to result queue overflow.")

            results_raw = message.split()[1:5]
            try:
                results = [int(number) for number in results_raw]
            except ValueError:
                return False

            sort = "Primary" if b'P' in message else "Secondary"

            return sort, results[:self.result_number]

        if b'MSG:' in message:
            message = str(message)
            message_code = int(message.split()[1])

            logging.info(MSG_DICT[message_code])

            if message_code == 1 and message.split()[2] == '0':
                logging.info("The reconfiguration was successful.")

            return True

        if b'STAT' in message:
            average_raw = message.split()[1:5]
            avg_values = [int(number) for number in average_raw]

            deviation_raw = message.split()[6: 10]
            std_dev = [float(number) for number in deviation_raw]

            sort = "Primary" if b'STAT:P' in message else "Secondary"

            logging.info(f"{sort} statistics: average: {avg_values}, standard deviation {std_dev}")

            return avg_values, std_dev

        if b'R:' in message:
            message = str(message)
            error_msg = message.split()[2]
            error_code = int(error_msg.split("=")[1].split("\\")[0])

            if error_code != 0:
                raise WieserLabsIPD4BException(error_code)

            return True

        logging.error(f"Unknown response {message}")
        return False

    @staticmethod
    def decoding_response_queue(message):

        if message.startswith("b'R:"):
            error_msg = message.split()[2]
            error_code = int(error_msg.split("=")[1])

            if error_code != 0:
                raise WieserLabsIPD4BException(error_code)

            return True

        logging.error("Unknown response")
        return False

    def read_command(self):
        while True:
            result = self.dev.readline()
            response = self.decoding_result_queue(result)
            if isinstance(response, tuple):
                self.read_queue.put(response)

    def get_values_primary(self):
        values = []
        level, value = self.read_queue.get()
        if level == "Primary":
            values.append(value)
        while not self.read_queue.empty():
            level, value = self.read_queue.get()
            if level == "Primary":
                values.append(value)

        np_values = np.array(values)

        for idx in range(np_values.shape[1]):  # pylint: disable=unsubscriptable-object
            self.result[idx] += list(np_values[:, idx])

        return tuple(np_values.mean(axis=0))

    def get_values_t2(self):
        stat_value = []
        for idx in range(self.result_number):
            result = self.result[idx]
            try:
                stdev_result = np.std(result)
                mean_result = np.mean(result)
                difference = mean_result - stdev_result
                sum_mean_stdev = mean_result + stdev_result
                stat_value.append((min(result), difference, sum_mean_stdev, max(result)))
                self.result[idx] = []
            except RuntimeWarning:
                pass
        return stat_value

    def send_command(self, cmd):
        msg = cmd + "\r"
        self.dev.write(msg.encode())

    def reconfig(self):
        self.send_command(":rc")

    def set_integration_time(self, time, cont_mode=False):

        if not cont_mode:
            if time in range(6, 1000001) and time not in range(351, 365):
                self.send_command(f":t {time}")
                logging.info(f"The integration time set to {time}")
                return True

            raise WieserLabsIPD4BException(112)

        if time in range(400, 1000001):
            self.send_command(f":t {time} c")
            logging.info(f"The integration time set to {time}")
            return True

        raise WieserLabsIPD4BException(113)

    def set_trigger_delay(self, time):
        if time in range(100000001):
            self.send_command(f":dly {time}")
            logging.info(f"The trigger delay time set to {time}")
            return True

        raise WieserLabsIPD4BException(114)

    def set_external_trigger_polarity(self, trend):
        if trend == "r":
            self.send_command(":etp r")

            logging.info("Set external trigger polarity to rising")
            return True

        if trend == "f":
            self.send_command(":etp f")

            logging.info("Set external trigger polarity to falling")
            return True

        raise WieserLabsIPD4BException(115)

    def set_result_mask(self, secondary=False, messages=True):

        if not secondary:
            rmask_value = "0x12" if messages else "0x02"
            self.send_command(f":rmask {rmask_value}")

            return True

        rmask_value = "0x14" if messages else "0xff"
        self.send_command(f":rmask {rmask_value}")

        return True

    def set_internal_trigger_mode(self, mode):

        if mode == "off":
            logging.info("Internal trigger set to: no internal trigger")

        elif mode == "per":
            logging.info("Internal trigger set to: periodic internal trigger.")

        elif mode == "dly":
            logging.info("""Internal trigger set to: user
                        internal trigger timer to provide an extended external trigger delay.""")

        else:
            raise WieserLabsIPD4BException(116)

        self.send_command(f":itm {mode}")
        return True

    def set_internal_trigger_period(self, period, prescaler=0):
        if period not in range(0, 65536):
            raise WieserLabsIPD4BException(117)

        if prescaler != 0:
            if prescaler not in range(1, 4001):
                raise WieserLabsIPD4BException(118)

            self.send_command(f":itp {period} {prescaler}")

            return True

        self.send_command(f":itp {period}")

        return True

    def set_full_scale_range(self, scale):

        if scale not in range(1, 8):
            raise WieserLabsIPD4BException(119)

        self.send_command(f":range {scale}")

        return True

    def stop_integration(self):
        self.send_command(":stop")
        return True

    def continue_integrator(self):
        self.send_command(":cont")
        return True

    def reset(self):
        self.send_command(":reset")
        return True

    def get_version(self):
        self.send_command(":version")
        return True

    def test_mode(self):
        self.send_command(":test")
        return True

    def get_stats(self, results_nos):
        if results_nos not in range(1, 10001):
            raise WieserLabsIPD4BException(120)
        self.send_command(f":istat {results_nos}")
        return True
