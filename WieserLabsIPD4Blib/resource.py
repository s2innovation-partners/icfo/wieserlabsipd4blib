ERROR_DICT = {
    1: "Argument out of range",
    2: "Missing argument for command",
    3: "Too many arguments for command",
    4: "Wrong number of arguments for command",
    5: "Unknown command",
    6: "Argument format error",
    111: "Device not found",
    112: "Integration time for PS mode must be in range 6 to 1000000 us excluding 351 to 364 us.",
    113: "Integration time for CONT mode must be in range 6 to 1000000 us.",
    114: "Time for trigger delay must be in range 0 to 100000000 us",
    115: "External trigger polarity can be only rising(r) or falling(f).",
    116: "Internal trigger mode can be off, per or dly.",
    117: "Internal trigger period must be in range 0 to 65535 us.",
    118: "Internal trigger prescaler must be in range 1 to 4000 us.",
    119: "Full scale range must be in range 1 to 7.",
    120: "Numbers of results for which statistic will be emitted must be in range 1 to 10000"
}

MSG_DICT = {
    0: "No message",
    1: """The reconfig has occurred. 
        All results after this message will be generated with the new parameter""",
    2: "Timeout"
}


class WieserLabsIPD4BException(Exception):
    def __init__(self, answer):
        try:
            self.msg = ERROR_DICT[answer]
        except KeyError:
            self.msg = "Unknown error"
        Exception.__init__(self, self.msg)

    def __str__(self):
        return self.msg
